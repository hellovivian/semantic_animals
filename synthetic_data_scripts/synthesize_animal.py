import bpy

import bpy
import os
from random import random, randrange
import numpy as np
import pickle

import mathutils
import math

from mathutils import Euler
from math import radians
import bmesh
import open3d as o3d
from itertools import product


scene = bpy.context.scene

#Set material for metaballs
mat = bpy.data.materials.new(name='SphereMaterial')
mat.diffuse_color = (0.0, 1.0, 0.0, 1.0)


#Credit: ranahanocka/MeshCNN 
#Function: Subdivides mesh
def subsurf(mesh):
        # subdivide mesh
        bpy.context.view_layer.objects.active = mesh
        mod = mesh.modifiers.new(name='Subsurf', type='SUBSURF')
        mod.subdivision_type = 'SIMPLE'
        bpy.ops.object.modifier_apply(modifier=mod.name)
        # now triangulate
        mod = mesh.modifiers.new(name='Triangluate', type='TRIANGULATE')
        bpy.ops.object.modifier_apply(modifier=mod.name)
        
#Credit: ranahanocka/MeshCNN   
#Function: Simplifies to a target number of faces per mesh
def simplify(mesh, target_faces):
        bpy.context.view_layer.objects.active = mesh
        mod = mesh.modifiers.new(name='Decimate', type='DECIMATE')
        bpy.context.object.modifiers['Decimate'].use_collapse_triangulate = True
        #
        nfaces = len(mesh.data.polygons)
        if nfaces < target_faces:
            subsurf(mesh)
            nfaces = len(mesh.data.polygons)
        ratio = target_faces / float(nfaces)
        mod.ratio = float('%s' % ('%.6g' % (ratio)))
        print('faces: ', mod.face_count, mod.ratio)
        bpy.ops.object.modifier_apply(modifier=mod.name)

    
            

#Class generates animals based on semantic parameters.
class Template_Animal():
    
    def __init__(self, torso_length, neck_length, neck_rotation, leg_length, tail_length, tail_rotation):
        self.global_shape = None
        self.torso_length = torso_length
        self.neck_length = neck_length
        self.neck_rotation = neck_rotation
        self.leg_length = leg_length
        self.tail_length = tail_length
        self.tail_rotation = tail_rotation
        
        self.torso_radius = 1.2
        self.head_size = 0.7
        self.neck_size = 0.4
        self.tail_radius = 0.5
        self.frontlegs_posx = 1
        self.backlegs_posx = -1
        self.leg_gap=1
        self.leg_radius=0.5
        
        
        self.generate_shape()
    
    def generate_shape(self):
        self.make_body()
        self.make_neck()

        self.make_legs(self.frontlegs_posx)
        self.make_legs(self.backlegs_posx)
        self.make_tail()
        
#      
    def make_body(self):
        add_metaball = bpy.ops.object.metaball_add
        self.global_shape = add_metaball(type='ELLIPSOID', enter_editmode=True, location=(0,0,0), radius=1)
        self.global_shape = bpy.context.active_object.data
        self.global_shape.threshold= 0.05
#        self.global_shape.elements[-1].size_x = self.torso_length
        element = self.global_shape.elements.new(type="BALL")
        element.co = (self.torso_length/4, 0,0)
        element.size_x = self.torso_length /2
        element.radius = self.torso_radius
        
        element = self.global_shape.elements.new(type="BALL")
        element.co = (-self.torso_length/4, 0,0)
        element.size_x = self.torso_length /2
        element.radius = self.torso_radius
        

      

    def make_neck(self):
        element = self.global_shape.elements.new(type="CAPSULE")
        element.rotation = Euler((0, radians(self.neck_rotation), 0), 'XYZ').to_quaternion()
        
        
        #place neck at edge of torso
        x_displacement = self.neck_length/2
        x_neckoffset = np.cos(radians( 180-self.neck_rotation)) * x_displacement *0.5
        z_neckoffset = np.sin(radians( 180-self.neck_rotation)) * x_displacement *0.5
        element.co=(self.torso_length/2 + x_neckoffset, 0, z_neckoffset)
        
#        element.co = (self.torso_length/2, 0,  0)
        element.size_x = self.neck_length/2
        element.size_z = self.neck_size
        element.size_y = self.neck_size
        element.radius = self.neck_size
        
        element = self.global_shape.elements.new(type="BALL")
        element.rotation = Euler((0, radians(self.neck_rotation), 0), 'XYZ').to_quaternion()
#        
        x_displacement=  self.neck_length
        x_headoffset = np.cos(radians( 180-self.neck_rotation)) * x_displacement
        z_headoffset = np.sin(radians( 180-self.neck_rotation)) * x_displacement
#        
        element.co = (self.torso_length/2 + x_headoffset, 0,  z_headoffset)
        element.size_x = self.neck_length
        element.size_z = self.head_size
        element.size_y = self.head_size
        element.radius = self.head_size
        

        
        

    def make_legs(self, torso_placement):
        element = self.global_shape.elements.new(type="CAPSULE")
        element.rotation = Euler((0, radians(90), 0), 'XYZ').to_quaternion()
        element.co = (torso_placement,self.leg_gap,-self.leg_length/4)
        element.radius= self.leg_radius
        element.size_z = self.leg_length/2
        element = self.global_shape.elements.new(type="CAPSULE")
        element.rotation = Euler((0, radians(90), 0), 'XYZ').to_quaternion()
        element.co = (torso_placement,self.leg_gap,-self.leg_length/2)
        element.radius= self.leg_radius
        element.size_z = self.leg_length/2
        
        
        element = self.global_shape.elements.new(type="CAPSULE")
        element.radius = self.leg_radius
        element.size_z = self.leg_length /2
        element.rotation = Euler((0, radians(90), 0), 'XYZ').to_quaternion()
        element.co = (torso_placement,-self.leg_gap,-self.leg_length/4)
        element = self.global_shape.elements.new(type="CAPSULE")
        element.radius = self.leg_radius
        element.size_z = self.leg_length/2
        element.rotation = Euler((0, radians(90), 0), 'XYZ').to_quaternion()
        element.co = (torso_placement,-self.leg_gap,-self.leg_length/2)
        

    def make_tail(self):
        
        
        element = self.global_shape.elements.new(type="CAPSULE")
        element.rotation = Euler((0, radians(self.tail_rotation), 0), 'XYZ').to_quaternion()
        
        
        #place neck at edge of torso
        x_displacement = self.tail_length/2
        x_tailoffset = np.cos(radians( 180-self.neck_rotation)) * x_displacement
        z_tailoffset = np.sin(radians( 180-self.neck_rotation)) * x_displacement
        element.co=(-self.torso_length/2, 0,0)
        
#        element.co = (self.torso_length/2, 0,  0)
        element.size_x = self.tail_length/2
        element.size_z = self.tail_radius
        element.size_y = self.tail_radius
        element.radius = self.tail_radius
        

    def get_mesh(self):
        bpy.data.objects['Mball'].select_set(True)
        depsgraph = bpy.context.evaluated_depsgraph_get()
        mesh = bpy.data.objects['Mball'].evaluated_get(depsgraph).to_mesh()
        return mesh
       
    #Credit: ambi on blender forum
    def read_verts(self,mesh):
            mverts_co = np.zeros((len(mesh.vertices)*3), dtype=np.float)
            mesh.vertices.foreach_get("co", mverts_co)
            return np.reshape(mverts_co, (len(mesh.vertices), 3))       

#Creating sets up parameters and perturbed parameters to generate pairs of synthetic pointclouds.

import numpy as np
import random
random.seed(1)
parameter_ranges = [[2,6],[2,3],[90,180],[1,2],[1,7],[-30,30]]




def generate_parameter_pairs(n=10000):
    semantic_param_pairs = np.zeros((2,n,6))
    
    for i in range(n):
        
        #Generates random numbers within the allowed parameter ranges above.
        random_param_base = [random.uniform(param_range[0], param_range[1]) for param_range in parameter_ranges]
        
        #Chooses one axis to perturb, and edits it 
        axis_to_edit = random.choice(range(6))
        target_value = random.uniform(parameter_ranges[axis_to_edit][0], parameter_ranges[axis_to_edit][1])
        random_param_edit = random_param_base.copy()
        random_param_edit[axis_to_edit] = target_value

        semantic_param_pairs[0][i] = random_param_base
        semantic_param_pairs[1][i] = random_param_edit
        
    return semantic_param_pairs
    


#Generate a random animal
def generate_random_animal(params):
    return Template_Animal(torso_length=params[0], neck_length=params[1],neck_rotation=params[2], leg_length=params[3],tail_length=params[4], tail_rotation=params[5])

#Placeholder to fill in with synthetic data 
synthetic_training_data = np.zeros((2,10000,2048,3))

#Normalization function that rescales and recenters pointclouds using open3d, a Python library for 3D computer graphics operations
def rescale_recenter_ptcloud(ptcloud_pts,scale=0.8):
    
    #Scaling to fit within unit cube
    positive_pcd = ptcloud_pts + np.abs(np.min(ptcloud_pts))
    rescaled_ptcloud = (positive_pcd / max(np.max(positive_pcd, axis=0) - np.min(positive_pcd, axis=0)))

    #Centering at 3D origin
    pcd = o3d.geometry.PointCloud(o3d.utility.Vector3dVector(rescaled_ptcloud))
    pcd = pcd.scale(scale,pcd.get_center())
    pcd = pcd.translate(-1 * pcd.get_center())

    return np.asarray(pcd.points) * scale

#Takes the mesh outputted by the TemplateAnimal class above and outputs a pointcloud, also utilizing Python
def generate_synthetic_ptcloud(params):
    synthetic_animal = generate_random_animal(params)
    synthetic_animal_mesh = synthetic_animal.get_mesh()
    synthetic_animal_mesh.calc_loop_triangles()
    
    synthetic_animal_triangles = np.zeros((len(synthetic_animal_mesh.loop_triangles),3), dtype=np.int)
    
    #Set all mesh loop polygons to be triangles, then loop through them to fill with vertices
    for tri_index in range(len(synthetic_animal_mesh.loop_triangles)):
    
        tri_vertices = synthetic_animal_mesh.loop_triangles[tri_index].vertices
        synthetic_animal_triangles[tri_index] = np.array([tri_vertices[0], tri_vertices[1], tri_vertices[2]])
        
    #Set vertices and triangles of an Open3D mesh
    mesh_vertices = o3d.utility.Vector3dVector(synthetic_animal.read_verts(synthetic_animal_mesh))
    mesh_triangles = o3d.utility.Vector3iVector(synthetic_animal_triangles)  
    o3d_mesh = o3d.geometry.TriangleMesh(mesh_vertices, mesh_triangles)
    
    #Sample open3D mesh to create a pointcloud
    o3d_ptcloud = o3d_mesh.sample_points_uniformly(2048)
    
    #Normalize pointcloud
    rescale_recenter_ptcloud(o3d_ptcloud.points, scale=1)
    
    #Delete original object in Blender scene
    bpy.data.objects.remove(bpy.context.active_object, do_unlink=True)
    return np.asarray(o3d_ptcloud.points)

#Generate pairs on the order of 10000, fill the placeholder array above
parameter_pairs = generate_parameter_pairs()

for i in range(0,10000):
    base = parameter_pairs[0,i,:]
    edit = parameter_pairs[1,i,:]
    
    synthetic_training_data[0,i] = generate_synthetic_ptcloud(base)
    synthetic_training_data[1,i] = generate_synthetic_ptcloud(edit)
    
    if i % 100 == 0:
        print(i)

#Export the generated synthetic data in pickle files
with open("parameter_pairs.pkl", "wb") as f:
    pickle.dump(parameter_pairs,f)
    
with open("norm_ptcloud_pairs.pkl", "wb") as f:
    pickle.dump(synthetic_training_data,f)
