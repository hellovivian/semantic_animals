# vl2463 | coms4995 final project 

<p align="center">
    
:lion:  Objective: Train a generative network that makes 3D animal shapes. :dog:
:elephant:
:racehorse:
:koala:
:tiger2:


## Instructions for Pulling

<li>
    -  Pull this repository. <br>
    -  On a Terminal run `docker build .` <br>
    -  The container will build and this may take a while to pull and download. <br>
    -  Afterwards, ideally you will see the terminal output `Successfully built CONTAINERNAME` <br>
    - Run the docker container using the following flags. `docker run -it --gpus all -p 5000:5000 --ipc=host  CONTAINERNAME "bash"` <br>
    - Ideally, you will see yourself in the container as root@CONTAINERNAME, in the `/home` directory. <br>
    - If you run `ls` you should see this repository pulled down as `semantic_animals`. <br> 
    - Please activate a conda environment using the command `conda activate ai_clouds`. <br>
    - Please run the following command `jupyter notebook --ip 0.0.0.0 --no-browser --allow-root --port=5000` <br>
    - You should see a link you can paste into a web browser.<br>
</li>

## Instructions for Training

<li>
   - Open `Semantic_Net.ipynb` in the the training_scripts folder in the Jupyter directory.<br>
   - Run the cells.<br>
   - You can confirm that it trains and you can also load saved weights.<br>
   - You can plot out some of the 3D outputs with the functions provided and toggle parameters as you like.<br>
   - You can also another notebook in the `model_exports_weights` folder for how I converted the model between Python kernels / neural net formats <br>
</li>


## Directory
<li>

- structured_losses: CUDA-accelerated loss function in Python and CUDA, shared libraries, and symbolic links
- model_export_weights: Exported weights, universal format neural network (ONNX), and the ipython notebook I used for conversion from Keras to onnx
- training_scripts: Scripts with my model architecture in both Keras as well as Pytorch
- synthetic_data scripts: Scripts I wrote in Blender to create synthetic data in the thousands.
</li>

## Attributions

<li>
-Everything in the structured_losses folder, which is the Python 2 implementation of Earth Mover's Distance is from @optas: https://github.com/optas/latent_3d_points <br>

-The ideas behind the architecture come from Learning to Infer Semantic Parameters for 3D Shape Editing Wei et. al (2018) <br>

-Various other authors are credited throughout the scripts for providing functions related to blender: @ambi from Blender forums (for a vertex reading function), and @ranahanocka for providing mesh simplication functions for Blender via MeshCNN's github repo - https://github.com/ranahanocka/MeshCNN <br>

-My real dataset also comes from David O'Reilly (davidoreilly.com), and all the real data I worked with throughout this project came from Sketchfab. (sketchfab.com) <br>

</li>
