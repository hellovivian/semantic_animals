import open3d as o3d
import numpy as np
from pointcloud_helpers import *

import keras
from keras.losses import MSE
from keras.layers import Lambda, Input, Dense, Conv1D, Conv2D, MaxPooling1D, BatchNormalization, MaxPooling2D, Flatten, Reshape, UpSampling2D, Dropout
from keras.models import Model, Sequential,load_model
from keras import callbacks
from keras import backend as K

config = K.tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)

K.clear_session()
semantic_net = load_model("../data/iter2_semantic_decoder_dropout_corrected_shuffle_45epochs.h5", custom_objects={ 'emd': emd})

decoder = semantic_net.layers[-1].layers[-1]
encoder = semantic_net.layers[-1].layers[-2]

X_test = np.zeros((1,1,1,23)) + np.random.rand(23)
# X_test[0,0,:] = np.array([0.2,0.3,0.3,0.4,0.5,0.5])
predicted = semantic_net.layers[-1].layers[-1].predict(X_test).reshape(2048,3)


pcd = o3d.geometry.PointCloud()
pcd.points = o3d.utility.Vector3dVector(predicted.reshape(2048,3))
pcd.estimate_normals()

# estimate radius for rolling ball
distances = pcd.compute_nearest_neighbor_distance()
avg_dist = np.mean(distances)
radius = 1.5* avg_dist   

pcd.normals = o3d.utility.Vector3dVector(np.zeros(
    (1, 3)))  # invalidate existing normals

pcd.estimate_normals()

tetra_mesh, pt_map = o3d.geometry.TetraMesh.create_from_point_cloud(pcd)

mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_alpha_shape(pcd,0.03, tetra_mesh, pt_map)
mesh.compute_vertex_normals()


# mesh, densities = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(
#         pcd, depth=10)


# mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(
#            pcd,
#            o3d.utility.DoubleVector([radius, radius * 2]))

# mesh_tris = np.asarray(mesh.triangles)
# mesh_tris = mesh_tris.reshape(mesh_tris.shape[0] * mesh_tris.shape[1]).tolist()
# mesh_tris = ",".join([str(pt) for pt in mesh_tris])

o3d.visualization.draw_geometries([mesh])
