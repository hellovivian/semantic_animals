import os
import sys

os.system("cd external_v/structural_losses; echo 'making'; make; sudo apt-get install python-tk")

sys.path.append('../')
print('Done compiling CUDA scripts')

import numpy as np
import pickle
from keras.layers import Lambda, Input, Dense, Conv1D, Conv2D, MaxPooling1D, BatchNormalization, MaxPooling2D, Flatten, Reshape, UpSampling2D
from keras.models import Model, Sequential,load_model
from keras import backend as K

import random
import matplotlib

import os.path as osp

import matplotlib.pyplot as plt
import tensorflow as tf
import emd

import emd.structural_losses.tf_nndistance as tf_nn
import emd.structural_losses.tf_approxmatch as approx

from mpl_toolkits.mplot3d import Axes3D # <--- This is important for 3d plotting 


"""
EMD stands for Earth's mover's distance. This is the loss function that 
differences in point clouds against one another.
"""
#loss function
def emd(y_pred, y_true):
    y_pred = tf.reshape(y_pred, [-1 , 2048, 3])
    y_true = tf.reshape(y_true, [-1, 2048, 3])
    match = approx.approx_match(y_pred, y_true)
    emdloss = tf.reduce_mean(approx.match_cost(y_pred, y_true, match))
    return emdloss

"""
This is from @optas
It is a functionality for plotting out point clouds in matplotlib3d

"""
def plot_3d_point_cloud(x, y, z, show=True, show_axis=True, in_u_sphere=False, marker='.', s=8, alpha=.8, figsize=(5, 5), elev=10, azim=240, axis=None, title=None, *args, **kwargs):

    if axis is None:
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111, projection='3d')        
    else:
        ax = axis
        fig = axis

    if title is not None:
        plt.title(title)

    sc = ax.scatter(x, y, z, marker=marker, s=s, alpha=alpha, *args, **kwargs)
    ax.view_init(elev=elev, azim=azim)

    if in_u_sphere:
        ax.set_xlim3d(-0.5, 0.5)
        ax.set_ylim3d(-0.5, 0.5)
        ax.set_zlim3d(-0.5, 0.5)
    else:
        miv = 0.7 * np.min([np.min(x), np.min(y), np.min(z)])  # Multiply with 0.7 to squeeze free-space.
        mav = 0.7 * np.max([np.max(x), np.max(y), np.max(z)])
        ax.set_xlim(miv, mav)
        ax.set_ylim(miv, mav)
        ax.set_zlim(miv, mav)
        plt.tight_layout()

    if not show_axis:
        plt.axis('off')

    if 'c' in kwargs:
        plt.colorbar(sc)

    if show:
        plt.show()

    return fig


def plot_ptcloud(points):
    plot_3d_point_cloud(points[:,0], points[:,1], points[:,2])
